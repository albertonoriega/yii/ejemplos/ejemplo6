<?php

use app\models\Prendas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Prendas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prendas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Prendas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'referencia',
            'precio',
            [
                // En la carpeta web/imgs hemos colocado varias imagenes de prendas
                // en lugar de mostrar el nombre literal de la foto queremos que se muestre la imagen
                'label' => 'Foto Prenda',
                'attribute'=>'foto',
                'content' => function ($dato) {
                    return Html:: img("@web/imgs/{$dato->foto}",["width"=> 200]);
                }
            ],
            'portada',
            'oferta',
            'descuento',
            'categoria',
            'categoria0.tipo',
            'categoria0.subtipo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Prendas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
