<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this View */

$this->title = 'Visualizacion de arrays y CRUD de tablas';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo 6</h1>

        <p class="lead">Visualizacion de arrays y CRUD de tablas</p>

     
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Ejercicio1</h2>
                <div>
                   
                    Mostrar en pantalla los números pares del 1 al 100 
                     <ul>
                    <li>Creamos el array con un for </li> 
                    <li>Se lo mandamos a la vista </li>
                    <li>Lo mostramos con Html::ul</li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 1", // label
                        ["site/ejercicio1"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio2</h2>
                <div>
                    
                    Mostrar un array enumerado de arrays asociativos   
                    <ul>
                    <li>Para realizarlo utilizo un Html:: ul y un foreach </li> 
                    <li>Creo una subvista para mostrar cada registro </li>
                    <li>En la subvista hacemos uso de la clase card de bootstrap para mostrar los registros </li>
                    
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 2", // label
                        ["site/ejercicio2"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio3</h2>
                <div>
                    
                    Mostrar un array enumerado de arrays asociativos
                    <ul>
                    <li>Convertimos el array en un DataProvider con ArrayDataProvider </li> 
                    <li>Se lo mandamos a la vista y lo mostramos con un GRIDVIEW </li>
                    <li>Añadimos un botón que nos lleva al ejercicio 5</li>
                    
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 3", // label
                        ["site/ejercicio3"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio4</h2>
                <div>
                    
                    Igual que el ejercicio 3 pero con un array diferente
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 4", // label
                        ["site/ejercicio4"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio5</h2>
                <div>
                    
                    Mostrar un array enumerado de arrays asociativos
                    <ul>
                    <li>Convertimos el array en un DataProvider </li> 
                    <li>Se lo mandamos a la vista y lo mostramos con un LISTVIEW </li>
                    <li>Al ser LISTVIEW necesitamos una subvista para mostrar cada campo del array </li>
                    <li>Añadimos un botón que nos lleva al ejercicio 3</li>
                    
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 5", // label
                        ["site/ejercicio5"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio6</h2>
                <div>
                    
                    Igual que el ejercicio 5 pero con un array diferente
                                     
                   
                </div>
                <p><?= Html::a("Ejercicio 6", // label
                        ["site/ejercicio6"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio7</h2>
                <div>
                    
                    Mostrar un array enumerado de arrays asociativos obtenido de un API REST
                    <ul>
                    <li>Solicitamos mediante file_get_contents noticias</li>
                    <li>Convertimos el JSON en un array bidimensional (json_decode) </li> 
                    <li>Convertimos el array en un DataProvider </li> 
                    <li>Se lo mandamos a la vista y lo mostramos con un GRIDVIEW </li>
                    <li>En el GRIDVIEW además de mostrar los datos hacemos lo siguiente: </li>
                    <ul>
                    <li>Ordenación de campos =>Serial column</li>
                    <li>Insertamos los campos, en el orden que queramos y cambiamos su label</li>
                    <li>Insertamos imagenes haciendo uso del helper img</li>
                    <li>Insertamos un enlace dentro de un botón haciendo uso del helper url</li>
                    <li>Modificamos la tabla con bootstrap => tableoptions</li>
                    <li>Cambiamos la estructura del gridview => layout</li>
                    </ul>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 7", // label
                        ["site/ejercicio7"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio8</h2>
                <div>
                    
                    Igual que el ejercicio 8 pero cogemos los datos de otro API
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 8", // label
                        ["site/ejercicio8"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio9</h2>
                <div>
                    
                    Mostrar la tabla fotos a través de un GRIDVIEW
                    <ul>
                        <li>Creamos un active query de fotos</li>
                        <li>Creamos un dataProvider del activeQuery con ActiveDataProvider</li>
                        <li>Lo mostramos en la vista con un GRIDVIEW</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 9", // label
                        ["site/ejercicio9"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio10</h2>
                <div>
                    
                    Modificar un registro de fotos utilizando ActiveRecord-
                    <ul>
                        <li>Cambiar la Idprenda de la primera foto de forma aleatoria</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 10", // label
                        ["site/ejercicio10"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio11</h2>
                <div>
                    
                    Mostrar un registro de fotos en un DetailView
                    <ul>
                        <li>Crear un modelo de ActiveRecord para mandarle al widget</li>
                        <li>El modelo realizamos de dos formas:</li>
                        <ul>
                            <li>Utilizamos el método one() de un activeQuery (te devuelve el primer registro)</li>
                            <li>Utlizamos el método all() de un activeQuery (te devuelve un array de modelos) y te quedas con el registro que quieras</li>
                        </ul>
                        
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 11", // label
                        ["site/ejercicio11"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio12</h2>
                <div>
                    
                    Mostrar la tabla Fotos en un GridView utilizando CreateCommand
                    <ul>
                        <li>Utilizando el objeto de acceso a datos ejecuto una consulta SQL</li>
                        <li>Mando el resultado de la consulta, un array bidimensional al ArrayDataProvider</li>
                        <li>Mando el dataProvider al GridView</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 12", // label
                        ["site/ejercicio12"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio13</h2>
                <div>
                    
                    Mostrar la tabla Fotos en un GridView utilizando SQLDataProvider
                    <ul>
                        <li>Creo el DataProvider utilizando la clase SqlDataProvider y el código SQL de la consulta</li>
                        <li>Mando el DataProvider al GridView</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 13", // label
                        ["site/ejercicio13"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio14</h2>
                <div>
                    
                   Lo mismo que el ejercicio 12 pero para Prendas en lugar de Fotos
                  
                </div>
                <p><?= Html::a("Ejercicio 14", // label
                        ["site/ejercicio14"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio15</h2>
                <div>
                    
                   Lo mismo que el ejercicio 13 pero para Prendas en lugar de Fotos
                  
                </div>
                <p><?= Html::a("Ejercicio 15", // label
                        ["site/ejercicio15"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio16</h2>
                <div>
                    
                   Lo mismo que el ejercicio 9 pero para Prendas en lugar de Fotos
                  
                </div>
                <p><?= Html::a("Ejercicio 16", // label
                        ["site/ejercicio16"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
          </div>  

    </div>
</div>
