<div class="card">
  <h5 class="card-header text-white bg-primary mb-3"><?= $model["idVenta"] ?></h5>
  <div class="card-body">
    <h5 class="card-title text-white bg-danger mb-3">Precio total: <?= $model["precioTotal"]?> euros</h5>
    <p class="card-text">Cantidad: <?= $model["cantidad"]?> unidades</p>
    <p class="card-text">Producto: <?= $model["producto"]?></p>
    <p class="card-text">Poblacion: <?= $model["poblacion"]?></p>
    
  </div>
</div>

