<?php

use yii\widgets\ListView;
use yii\helpers\Html;

// Boton que te lleva al ejercicio 3 (GridView)
echo  Html::a("Tabla", ["site/ejercicio4"],['class'=>'btn btn-dark text-success m-3']);

echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_ejercicio6",
    
]);