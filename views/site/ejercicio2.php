<?php

use yii\helpers\Html;

// Con Html::ul
echo Html::ul($visitas, [
    'item'=> function ($registro){
    return $this->render("_mostrar2",[
        "registro"=>$registro, // mando a la subvista un array asociativo con el registro a mostrar
    ]);
    }
]);

// Con un foreach
foreach($visitas as  $registro){
    echo $this->render("_mostrar2",[
        "registro"=>$registro,
    ]);
   
}

