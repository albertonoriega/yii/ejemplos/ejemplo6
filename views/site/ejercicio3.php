<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

// Enlace que cambia al ejercicio5 para mostrar los mismos datos en un ListView
// Lo hemos metido en un boton

echo  Html::a("Lista", ["site/ejercicio5"],['class'=>'btn btn-primary m-3']);

// Hacemos el mismo boton con Url to
?>
<div>
<a class= "btn btn-dark m-3 text-success" href="<?= Url::to(["site/ejercicio5"]) ?>">Lista</a>
</div>


<?php
echo GridView::widget([
    'dataProvider'=> $dataProvider,
]);
