<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    "dataProvider" => $dataProvider,
    "columns" => [
        ['class' => 'yii\grid\SerialColumn'], // le ponemos la ordenacion
        // fecha de publicacion
        [
            'label'=> 'Fecha de publicación',
            'attribute' => 'publishedAt',
        ], 
        // Foto
        [
            'label' => 'Foto',
            'content' => function ($dato){
                return Html::img($dato->urlToImage, ["width" => 200]);
            }
        ],
        //titulo
        [
            'label'=> 'Título',
            'attribute' => 'title',
        ], 
         // autor
        [
            'label'=> 'Autor de la noticia',
            'attribute' => 'author',
        ], 
        // Enlace
        [
            'label' => 'Enlace',
            'content' => function ($dato){
                return Html::a("Ir a la noticia", $dato->url, ["class" => "btn btn-dark text-info m-3"]);
            }
        ],
                
    ],
   
]);

