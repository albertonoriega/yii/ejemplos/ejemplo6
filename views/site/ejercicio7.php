<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    "dataProvider" => $dataProvider,
    // Para que no nos salgan todos los campos del array,
    // Introducimos la propiedad columns, y escribimos dentro de un array los campos que queremos mostrar
    "columns" => [
        ['class' => 'yii\grid\SerialColumn'], // inserta un numero para ordenar los campos
        // campos a mostrar
        "author",
        "title",
        // campo al que cambio el label ( en lugar de descripction ponemos Descripcion )
        [
            'attribute' => 'description',
            'label' => 'Descripcion',
        ],
        // campo de imagen
        // Con 'content'  
        [
            'label' => 'Imagen',
            // mostramos la imagen de la siguiente forma:
            'content' => function ($dato) {
                return Html::img($dato->urlToImage, ["width" => 200]);
            },
        ],
        // Con 'format' y 'value'
        [
            'label' => 'Imagen 2',
            // con el format => raw podemos poner en la siguiente etique content o value
            // si no lo ponemos, con content muestra la foto y con value te escribe la etiqueta de la foto literal
            'format' => 'raw',
            // mostramos la imagen de la siguiente forma:
            'content' => function ($dato) {
                return Html::img($dato->urlToImage, ["width" => 200]);
            },
        ],
        // Campo que contiene una URL y quiero colocar un boton que me permita ir a la URL
        // Con 'content'         
        [
            'label' => 'Enlace',
            'content' => function ($dato) {
                return Html::a("Ir a la noticia", $dato->url, ["class" => "btn btn-light text-danger m-3"]);
            }
        ],
        // Con 'format' y 'value'        
        [
            'label' => 'Enlace 2',
            'format' => 'raw',
            'value' => function ($dato) {
                return Html::a("Ir a la noticia", $dato->url, ["class" => "btn btn-light text-danger m-3"]);
            }
        ],
    ],
    // Estilos visuales utilzando bootstrap
    'tableOptions' => ['class' => 'table table-hover table-dark table-striped table-bordered'],
    // Cambiando la estructura del gridview
    // pager -> te muestra la paginacion de la tabla
    // summary -> te muestra Mostrando 1-20 de 25 elementos
    // items -> te muestra el contenido de la tabla
    'layout' => "{pager}\n{summary}\n{items}\n{summary}\n{pager}",
]);
