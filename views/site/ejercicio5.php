<?php

use yii\helpers\Html;
use yii\widgets\ListView;

// Boton que te lleva al ejercicio 3 (GridView)
echo  Html::a("Tabla", ["site/ejercicio3"],['class'=>'btn btn-dark text-success m-3']);

echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_ejercicio5",
    
]);



