<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caracteristicas".
 *
 * @property int $id
 * @property int $prenda
 * @property string $caracteristica
 *
 * @property Prendas $prenda0
 */
class Caracteristicas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'caracteristicas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prenda', 'caracteristica'], 'required'],
            [['prenda'], 'integer'],
            [['caracteristica'], 'string', 'max' => 255],
            [['prenda', 'caracteristica'], 'unique', 'targetAttribute' => ['prenda', 'caracteristica']],
            [['prenda'], 'exist', 'skipOnError' => true, 'targetClass' => Prendas::class, 'targetAttribute' => ['prenda' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prenda' => 'Prenda',
            'caracteristica' => 'Caracteristica',
        ];
    }

    /**
     * Gets query for [[Prenda0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrenda0()
    {
        return $this->hasOne(Prendas::class, ['id' => 'prenda']);
    }
}
