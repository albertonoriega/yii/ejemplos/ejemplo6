<?php

namespace app\controllers;

use app\models\Caracteristicas;
use app\models\Fotos;
use app\models\Prendas;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /* 
     * Mostrando un array enumerado utilizando el helper de Html
     */ 
    public function actionEjercicio1(){
        
        // creo el array 
        $numeros=[];
        for($i=2;$i<101;$i=$i+2){
            $numeros[]=$i;
            //array_push($numeros,$i);
        }
        
        // muestro el array ( enumerado) con el render
        return $this->render('ejercicio1', [
             "numeros"=>$numeros,
             ]);
    }
    
    /* 
     * Mostrar un array enumerado de arrays asociativos
     * Para realizarlo utilizo un Html:: ul y un foreach
     * Creo una subvista para mostrar cada registro
     */ 
    public function actionEjercicio2 () {
        
        // Creo el array
        $visitas=[
            [
                "mes" => "Mayo",
                "visitas" => 10000,
            ],
            [
                "mes"=> "Junio",
                "visitas"=> 100000,
            ],
            [
                "mes"=> "Julio",
                "visitas"=> 200000,
            ],
            [
                "mes"=> "Agosto",
                "visitas"=> 300000,
            ]
        ];
        
        return $this->render('ejercicio2',[
            "visitas"=> $visitas,
        ]);
    }
    
    /*
     * Mostrar un array enumerado de arrays asociativos
     * Para mostrarlo usamos el widget GRIDVIEW
     */
    
    public function actionEjercicio3() {
        
        $visitas=[
            [
                "mes" => "Mayo",
                "visitas" => 10000,
            ],
            [
                "mes"=> "Junio",
                "visitas"=> 100000,
            ],
            [
                "mes"=> "Julio",
                "visitas"=> 200000,
            ],
            [
                "mes"=> "Agosto",
                "visitas"=> 300000,
            ]
        ];
        
        // renderizamos con un GridView
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $visitas,
        ]);
        
        return $this->render('ejercicio3',[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionEjercicio4 (){
        
        // Array enumerado de arrays asociativos
        
        $ventas = [
            [
                "idVenta" => 1,
                "cantidad" => 100,
                "producto" => 56,
                "precioTotal" => 560,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 2,
                "cantidad" => 300,
                "producto" => 563,
                "precioTotal" => 5600,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 3,
                "cantidad" => 200,
                "producto" => 50,
                "precioTotal" => 1600,
                "poblacion" => "Santander",
            ]
        ];
        
        
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $ventas,
        ]);
        
        return $this->render('ejercicio4',[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionEjercicio5 () {
        
        $visitas=[
            [
                "mes" => "Mayo",
                "visitas" => 10000,
            ],
            [
                "mes"=> "Junio",
                "visitas"=> 100000,
            ],
            [
                "mes"=> "Julio",
                "visitas"=> 200000,
            ],
            [
                "mes"=> "Agosto",
                "visitas"=> 300000,
            ]
        ];
        
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $visitas,
        ]);
        
        return $this->render('ejercicio5',[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionEjercicio6 () {
        
        $ventas = [
            [
                "idVenta" => 1,
                "cantidad" => 100,
                "producto" => 56,
                "precioTotal" => 560,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 2,
                "cantidad" => 300,
                "producto" => 563,
                "precioTotal" => 5600,
                "poblacion" => "Santander",
            ],
            [
                "idVenta" => 3,
                "cantidad" => 200,
                "producto" => 50,
                "precioTotal" => 1600,
                "poblacion" => "Santander",
            ]
        ];
        
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $ventas,
        ]);
        
        return $this->render('ejercicio6',[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    /*
     * Mostrar un array bidimensional que lo recibimos de un API REST
     * Para mostrar el array utilizamos el widget GRIDVIEW
     */
    public function actionEjercicio7() {
        // API KEY 
        // 3163d20f29c949a78e5a4ed308b8e821
                
        // Direccion del servidor API REST donde voy a solicitar datos
        $url= "https://newsapi.org/v2/everything?q=php&language=es&apiKey=3163d20f29c949a78e5a4ed308b8e821";
        // Funcion de PHP que te lee el contenido de un fichero de texto y te lo convierte a string
        $contenido=file_get_contents($url);
        // Funcion que te transforma un string de formato a un objeto
        $resultado= json_decode($contenido);
        // Del objeto, leo la propiedad articles que es un array de enumarado de arrays asociativos
        $noticias = $resultado->articles;
        
        // crear un dataprovider desde un array        
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $noticias,
        ]);
        
        return $this->render('ejercicio7',[
            "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionEjercicio8() {
        $url= "https://newsapi.org/v2/top-headlines?language=es&apiKey=3163d20f29c949a78e5a4ed308b8e821";
        $contenido = file_get_contents($url);
        $resultado = json_decode($contenido);
        $titulares = $resultado->articles;
        
        // Mostrar en un GridView en este orden: fecha de publicacion, foto, title, autor, enlace
        
        $dataProvider= new ArrayDataProvider([
            "allModels" => $titulares,
        ]);
        return $this->render('ejercicio8',[
            "dataProvider" => $dataProvider,
        ]);
    }
    
//    public function actionFotos () {
//        // Metodo para insetar una foto en la base de datos
//        $foto1= new \app\models\Fotos();
//        $foto1->id=2;
//        $foto1->ruta="foto2.jpg";
//        $foto1->idprenda=1;
//        // Para que conste en la base de datos hay que hacer un save y ejecutar la accion
//        $foto1->save();
//    }
    
    // A partir de aqui empezamos a trabajar con la base de datos
    
    /*
     * Motrar el contenido de una tabla en un GridView
     * Para ello vamos a utilizar un ActiveQuery (consulta activa)
     * Para crear una consulta activa necesito un modelo activo ( Active Record) 
     * Para poder visualizar el active query en un GRIDVIEW tengo que crear un DATAPROVIDER
     * Para crear el DATAPROVIDER utilizo un ActiveDataProvider
     */
    public function actionEjercicio9 () {
        // Active query (es una consulta sin hacer)
        $query= Fotos::find();
        
        
        // Para crear el dataProvider de una query usamos ActiveDataProvider
        $dataProvider = new ActiveDataProvider([
            "query" => $query,
        ]);
        
        return $this->render('ejercicio9', [
            'dataProvider' => $dataProvider,
        ]);
    }
    /*
     * Modificar un registro de fotos utilizando ActiveRecord-
     */
     public function actionEjercicio10 () {
        // Active query (es una consulta sin hacer)
        $query= Fotos::find();
         
        // Array de Active Records (array de modelos) Select * from fotos
        // El modelo mantiene una conexion con la BBDD
        $registros = $query->all();
                
        // Cambiamos el idprenda del primer registro del array. 
        $registros[0]->idprenda= random_int(1, 5);
        $registros[0]->save(); // modifica y guarda el cambio en la tabla fotos
        
        return $this->render('ejercicio10', [
            'query' => $query,
            'registros' => $registros,
        ]);
        
    }
    
    /*
     * Mostrar un registro de la tabla fotos utilizando el widget DetailView
     * Para que el DetailView funcione, solo necesito un modelo de ActiveRecord o un modelo o un array asociativo
     */
     public function actionEjercicio11 () {
        // IMPRESION DE UN SOLO REGISTRO
        
        // Active query
        $query= Fotos::find();
        
        // Active record (modelo) 
        $primeraFoto= $query->one(); // Accedemos a un solo registro (el primero con el metodo one) select * from fotos limit 1
          
        // Si es lugar del primer registro querriamos acceder a otro cualquiera:
        
        // Ejecutamos la consulta y nos devuelve todos los registros ( todas las fotos)
        $todasLasFotos=$query->all(); // Select * from fotos
        // De todos los registros nos quedamos con el segundo (segunda foto)
        $segundaFoto=$todasLasFotos[1];
        
        // Como solo tenemos un registro, lo mostramos usando DetailView
                
        return $this->render('ejercicio11',[
            "model"=> $segundaFoto, // A la vista le mando la foto2
            //"model" => $primeraFoto, // A la vista le mando la foto1
            // como es un DetailView, solo le podemos mandar 1
        ]);
    }
    
    
     /*
     * Motrar el contenido de una tabla en un GridView
     * Para ello vamos a utilizar el método CreateCommand del objeto de acceso a datos
      * Al ejecutar el método queryAll() te crea un array bidimensional
      * Mostrar ese array en el GridView creando un DataProvider
     */
     public function actionEjercicio12 () {
               
        //Este metodo solo se suele usar si la consulta a realizar es complicada y por lo tanto, no es óptimo hacerlo como en el ejrcicio9 ( Active Query)
        // Array enumerado de arrays asociativos
        $resultados= \yii::$app
                            ->db // objeto de acceso a datos
                            ->CreateCommand("select * from fotos") // comando sql preparado para ejecutarse
                            ->queryAll(); // consulta ejecutada
        // si modifico alguno de los registros no afecta a la tabla de la base de datos
        
        $dataProvider = new ArrayDataProvider([
            'allModels' => $resultados,
        ]);
        
        return $this->render('ejercicio12',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /*
     * Motrar el contenido de una tabla en un GridView
     * Para ello vamos a utilizar el SQLDatProvider
      * Introducimos el codigo sql de la consulta a ejecutar
      * Mostrar ese array en el GridView creando un DataProvider
     */
    public function actionEjercicio13 () {
        //Este metodo hace lo mismo que el ejercicio 12 por lo que también se suele usar si la consulta a realizar es complicada
        // Mostrar la siguiente consulta en un grid view
        // select * from fotos
        
        $dataProvider = new SqlDataProvider([
            'sql' => 'select * from fotos',
        ]);
        
        return $this->render('ejercicio12',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionEjercicio14 () {
        // Listado de todas las prendas utilazando createCommand
        // La consulta sería: select * from prendas
        // Utilizar para mostrar los resultados un grid view
        
        // NOTA: Acordarse de poner la contrabarra en la ruta para que no de error
        $query= \yii::$app
                            ->db // objeto de acceso a datos
                            ->CreateCommand("select * from prendas"); // comando sql preparado para ejecutarse
        
        $resultados= $query->queryAll(); // Array de arrays con todos los datos de la consulta ejecutada
        
        $dataProvider = new ArrayDataProvider([
            'allModels' => $resultados,
        ]);
        
        return $this->render('ejercicio14',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionEjercicio15 () {
        // Listado de todas las prendas utilazando sqldataprovider
        // La consulta sería: select * from prendas
        // Utilizar para mostrar los resultados un grid view
        
        $dataProvider = new SqlDataProvider([
            "sql" => "select * from prendas",
        ]);
        
        return $this->render('ejercicio14',[
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionEjercicio16 () {
        // Listado de todas las prendas utilazando un activedataprovider
        // Utilizar para mostrar los resultados un grid view
        
        // Active query (es una consulta sin hacer)
        $query= Prendas::find();
        
        $dataProvider = new ActiveDataProvider([
            "query" => $query,
        ]);
        
        return $this->render('ejercicio14', [
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    // Generar un array con el campo caracteristicas
    public function actionEjercicio17 () {
        
        
        // Consulta no ejecutada (activa)
        $query = Caracteristicas::find();
        
        
        // Array de modelos ( Hay conexion con la tabla original)
        $vector = $query->all();
        
        // Creamos un array de las caracteristicas ( Como tenemos un array de objetos tenemos que usar -> para accerder a las caracteristicas
//        foreach ($vector as $elemento){
//            $salida[]=$elemento->caracteristica;
//        }
        
        // Lo mismo que el foreach de arriba pero utilizando el ArrayHelper
        $salida = ArrayHelper::getColumn($vector, "caracteristica");
        
        var_dump($salida);
        
        // Array de arrays ( Aqui se pierde la conexion de la tabla original)
        
        $vector1= $query->asArray()->all();
        
        $salida = ArrayHelper::getColumn($vector, "caracteristica");
        
        var_dump($salida);
        
    }
}
    
